/**
 * @description       :
 * @author            : concret.io
 * @group             :
 * @last modified on  : 11-27-2021
 * @last modified by  : concret.io
 **/
public inherited sharing class HistoryFetchReportController{
    
    @AuraEnabled
    public static void fetchToRestore(){
        Datetime installationDatetime = DriftAppService.getInstallationDate();
        DriftConversionListCallout driftCallout = new DriftConversionListCallout();
        try{
            Id batchJobId = Database.executeBatch(driftCallout, 1);
            DriftAppService.storeFetchingJobId(batchJobId);
        } catch (Exception e){
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable = true)
    public static String getCurrentStatus(){
        return DriftAppService.getCurrentRestoringStatus();
    }
}