/**
 * @description       : 
 * @author            : concret.io
 * @group             : 
 * @last modified on  : 11-25-2021
 * @last modified by  : concret.io
**/
public inherited sharing class DriftConversationMessages {
    public class Context {
		public String ip;
		public Location location;
	}

	public class Pagination {
		public Boolean more;
		public String next;
	}

	public class Messages {
		public Long id;
		public Long conversationId;
		public String body;
		public Author author;
		public String type;
		public Long createdAt;
		public Context context;
		public Attributes attributes;
	}

	public Data data;
	public Pagination pagination;
	public Error error;

	public class Attributes {
	}

	public class Author {
		public Integer id;
		public String type;
		public Boolean bot;
	}

	public class Error {
		public String message;
		public String type;
	}

	public class Data {
		public List<Messages> messages;
	}

	public class Location {
		public String city;
		public String region;
		public String country;
		public String countryName;
		public String postalCode;
		public Double latitude;
		public Double longitude;
	}
	
	public static DriftConversationMessages parse(String json) {
		return (DriftConversationMessages) System.JSON.deserialize(json, DriftConversationMessages.class);
	}
}