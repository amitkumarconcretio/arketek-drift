/**
 * @description       : 
 * @author            : concret.io
 * @group             : 
 * @last modified on  : 11-25-2021
 * @last modified by  : concret.io
**/
@IsTest
public inherited sharing class DriftConversationMessagesTest {
    @IsTest
    static void testParse() {
		String json = '{'+
		'  \"data\": {'+
		'    \"messages\": ['+
		'      {'+
		'        \"id\": 12326521920,'+
		'        \"conversationId\": 3329601971,'+
		'        \"body\": \"hi Tricia\",'+
		'        \"author\": {'+
		'          \"id\": 5064679,'+
		'          \"type\": \"user\",'+
		'          \"bot\": false'+
		'        },'+
		'        \"type\": \"chat\",'+
		'        \"createdAt\": 1637761020195,'+
		'        \"context\": {'+
		'          \"ip\": \"43.230.64.194\",'+
		'          \"location\": {'+
		'            \"city\": \"Noida\",'+
		'            \"region\": \"Uttar Pradesh\",'+
		'            \"country\": \"IN\",'+
		'            \"countryName\": \"India\",'+
		'            \"postalCode\": \"110091\",'+
		'            \"latitude\": 28.6145,'+
		'            \"longitude\": 77.3063'+
		'          }'+
		'        },'+
		'        \"attributes\": {}'+
		'      },'+
		'      {'+
		'        \"id\": 12326522242,'+
		'        \"conversationId\": 3329601971,'+
		'        \"body\": \"How are you?\",'+
		'        \"author\": {'+
		'          \"id\": 5064679,'+
		'          \"type\": \"user\",'+
		'          \"bot\": false'+
		'        },'+
		'        \"type\": \"chat\",'+
		'        \"createdAt\": 1637761026029,'+
		'        \"context\": {'+
		'          \"ip\": \"43.230.64.194\",'+
		'          \"location\": {'+
		'            \"city\": \"Noida\",'+
		'            \"region\": \"Uttar Pradesh\",'+
		'            \"country\": \"IN\",'+
		'            \"countryName\": \"India\",'+
		'            \"postalCode\": \"110091\",'+
		'            \"latitude\": 28.6145,'+
		'            \"longitude\": 77.3063'+
		'          }'+
		'        },'+
		'        \"attributes\": {}'+
		'      },'+
		'      {'+
		'        \"id\": 12326522671,'+
		'        \"conversationId\": 3329601971,'+
		'        \"body\": \"This is just a sample text\",'+
		'        \"author\": {'+
		'          \"id\": 5064679,'+
		'          \"type\": \"user\",'+
		'          \"bot\": false'+
		'        },'+
		'        \"type\": \"chat\",'+
		'        \"createdAt\": 1637761033499,'+
		'        \"context\": {'+
		'          \"ip\": \"43.230.64.194\",'+
		'          \"location\": {'+
		'            \"city\": \"Noida\",'+
		'            \"region\": \"Uttar Pradesh\",'+
		'            \"country\": \"IN\",'+
		'            \"countryName\": \"India\",'+
		'            \"postalCode\": \"110091\",'+
		'            \"latitude\": 28.6145,'+
		'            \"longitude\": 77.3063'+
		'          }'+
		'        },'+
		'        \"attributes\": {}'+
		'      },'+
		'      {'+
		'        \"id\": 12326826105,'+
		'        \"conversationId\": 3329601971,'+
		'        \"body\": \"Is there anything I can help out with while you browse around? \",'+
		'        \"author\": {'+
		'          \"id\": 5064679,'+
		'          \"type\": \"user\",'+
		'          \"bot\": false'+
		'        },'+
		'        \"type\": \"chat\",'+
		'        \"createdAt\": 1637765004740,'+
		'        \"context\": {'+
		'          \"ip\": \"43.230.64.194\",'+
		'          \"location\": {'+
		'            \"city\": \"Noida\",'+
		'            \"region\": \"Uttar Pradesh\",'+
		'            \"country\": \"IN\",'+
		'            \"countryName\": \"India\",'+
		'            \"postalCode\": \"110091\",'+
		'            \"latitude\": 28.6145,'+
		'            \"longitude\": 77.3063'+
		'          }'+
		'        },'+
		'        \"attributes\": {}'+
		'      }'+
		'    ]'+
		'  },'+
		'  \"pagination\": {'+
		'    \"more\": true,'+
		'    \"next\": \"123133231\"'+
		'  },'+
		'  \"error\": {'+
		'        \"message\": \"The access token is invalid or has expired\",'+
		'        \"type\": \"authentication_error\"'+
		'    }'+
		'}';
		DriftConversationMessages obj = DriftConversationMessages.parse(json);
		System.assert(obj != null);
	}
}