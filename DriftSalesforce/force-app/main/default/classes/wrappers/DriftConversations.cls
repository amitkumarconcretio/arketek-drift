/**
 * @description       :
 * @author            : concret.io
 * @group             :
 * @last modified on  : 11-25-2021
 * @last modified by  : concret.io
 **/
public inherited sharing class DriftConversations{
    public class Pagination {
		public Boolean more;
		public String next;
	}

	public List<Data> data;
	public Pagination pagination;
	public Error error;

	public class Error {
		public String message;
		public String type;
	}

	public class Data {
		public String status;
		public Long contactId;
		public Long createdAt;
		public Long id;
		public Long inboxId;
		public Long updatedAt;
	}

	public static DriftConversations parse(String json) {
		return (DriftConversations) System.JSON.deserialize(json, DriftConversations.class);
	}
}