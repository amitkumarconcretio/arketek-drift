/**
 * @description       :
 * @author            : concret.io
 * @group             :
 * @last modified on  : 11-26-2021
 * @last modified by  : concret.io
 **/
public inherited sharing class DriftRequestBuilder{
    private static String CONVERSATIONS = 'https://driftapi.com/conversations/list';
    private static String CONVERSATION = 'https://driftapi.com/conversations/{CONVERSATION_ID}';
    private static String MESSAGES = 'https://driftapi.com/conversations/{CONVERSATION_ID}/messages';
    private static String CONTACT = 'https://driftapi.com/contacts/{CONTACT_ID}';
    private static String USERS = 'https://driftapi.com/users/list';
    private static String USER = 'https://driftapi.com/users/{USER_ID}';
    private static String ACCOUNTS = 'https://driftapi.com/accounts';
    private static String ACCOUNT = 'https://driftapi.com/accounts/{ACCOUNT_ID}';
    private static String PLAYBOOK = 'https://driftapi.com/playbooks/list';
    private static String TOKEN;
    static{
        TOKEN = DriftAppService.getValue();
    }

    public static HttpRequest getConversationListRequest(String next){
        String endpoint = CONVERSATIONS+'?statusId=2&limit=40';
        if(next != null){
            endpoint += '&next='+next;
        }
        return getRequest(endpoint);
    }
    
    public static HttpRequest getUsersListRequest(String next){
        String endpoint = USER+'?limit=100';
        if(next != null){
            endpoint += '&next='+next;
        }
        return getRequest(endpoint);
    }

    public static HttpRequest getAccountsListRequest(String next){
        String endpoint = ACCOUNTS+'?limit=100';
        if(next != null){
            endpoint += '&next='+next;
        }
        return getRequest(endpoint);
    }

    public static HttpRequest getPlaybookListRequest(String next){
        String endpoint = PLAYBOOK+'?limit=100';
        if(next != null){
            endpoint += '&next='+next;
        }
        return getRequest(endpoint);
    }

    private static HttpRequest getRequest(String endpoint){
        HttpRequest request  = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('GET');
        request.setHeader('Authorization', 'Bearer '+TOKEN);
        return request;
    }
}