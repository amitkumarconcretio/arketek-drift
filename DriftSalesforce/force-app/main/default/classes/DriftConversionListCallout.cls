/**
 * @description       :
 * @author            : concret.io
 * @group             :
 * @last modified on  : 11-27-2021
 * @last modified by  : concret.io
 **/
public class DriftConversionListCallout implements Database.Batchable<Integer>, Database.AllowsCallouts, Database.Stateful{
    public String next = null;

    public Iterable<Integer> start(Database.BatchableContext context){        
        System.debug('new DrifConversationListIterable(2000)');
        return new DrifConversationListIterable(2000); 
        // return new DrifConversationListIterable(200000); 
    }

    public void execute(Database.BatchableContext context, Integer[] values){
        if (Cache.Org.contains('local.DriftHistory.NEXT')) {
            next = String.valueOf(Cache.Org.get('local.DriftHistory.NEXT'));
        }
        HttpRequest request = DriftRequestBuilder.getConversationListRequest(next);
        HttpResponse response = new Http().send(request);
        DriftConversations conversations = DriftConversations.parse(response.getBody());
        List<Map<String,Object>> conversionInfo = new List<Map<String,Object>>();
        if (response.getStatusCode() == 200 && conversations.data.size() != 0){
            for(DriftConversations.Data content : conversations.data){
                Map<String,Object> dataMap = new Map<String, Object>();
                dataMap.put('ConversationId',String.valueOf(content.id));
                dataMap.put('InboxId',String.valueOf(content.inboxId));
                dataMap.put('CreatedAt',Datetime.newInstance(content.createdAt));
                dataMap.put('UpdatedAt',Datetime.newInstance(content.updatedAt));
                conversionInfo.add(dataMap);
            }
            if (!conversionInfo.isEmpty())DriftAppService.storeRecordToRestore(conversionInfo, next);

            if (conversations.pagination != null && conversations.pagination.more == true){
                next = conversations.pagination.next;
            } else{
                System.AbortJob(context.getJobId());
            }
        }
    }

    public void finish(Database.BatchableContext context){

        Cache.Org.put('local.DriftHistory.NEXT', next);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Step 2: Set list of people who should get the email
        List<String> sendTo = new List<String>();
        sendTo.add('abhinav@concret.io');
        sendTo.add('kumar.amit@concret.io');
        sendTo.add('asifjamal@concret.io');        

        mail.setToAddresses(sendTo);
        
        // Step 3: Set who the email is sent from
        mail.setReplyTo('abhinav@concret.io');
        mail.setSenderDisplayName('Arketek Drift Importer');
        
        // (Optional) Set list of people who should be CC'ed
        List<String> ccTo = new List<String>();
        ccTo.add('mike@arketek.ca');
        mail.setCcAddresses(ccTo);
        
        // Step 4. Set email contents - you can use variables!
        mail.setSubject('Drift Bulk Loading Finished!');
        mail.setHtmlBody('Done loading, please check Salesforce now');
        
          // Step 6: Send all emails in the master list
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    }
}