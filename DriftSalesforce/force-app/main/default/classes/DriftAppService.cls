/**
 * @description       :
 * @author            : concret.io
 * @group             :
 * @last modified on  : 11-27-2021
 * @last modified by  : concret.io
 **/
public inherited sharing class DriftAppService{
    public static String getValue(){
        Id orgId = UserInfo.getOrganizationId();
        Drift_App__c appInfo = Drift_App__c.getInstance(orgId);
        return appInfo.Value__c;
    }

    public static Datetime getInstallationDate(){
        ApexTrigger at = [SELECT Id, NamespacePrefix, CreatedDate
                          FROM ApexTrigger
                          WHERE NamespacePrefix = 'DriftData'][0];
        return at.CreatedDate;
    }

    public static Boolean storeRecordToRestore(List<Map<String, Object>> conversationList, String next){
        Integer count = 0;
        if (next != null){
            count = Integer.valueOf(next);
        }
        List<DriftData__Drift_Conversation__c> driftConversationList = new List<DriftData__Drift_Conversation__c>();
        for (Map<String, Object> conversation : conversationList){
            count++;
            Decimal conversationId = Decimal.valueOf('' + conversation.get('ConversationId'));
            Decimal inboxId = Decimal.valueOf('' + conversation.get('InboxId'));
            Datetime startDatetime = Datetime.valueOf(conversation.get('CreatedAt'));
            Datetime endDatetime = Datetime.valueOf(conversation.get('UpdatedAt'));
            DriftData__Drift_Conversation__c driftConversation = new DriftData__Drift_Conversation__c(Name = 'HISTORY CONVERSATION ' + count, DriftData__Conversation_ID__c = conversationId, DriftData__Inbox_ID__c = inboxId, DriftData__Conversation_Start_Time__c = startDatetime, DriftData__Conversation_Close_Time__c = endDatetime);
            driftConversationList.add(driftConversation);
        }
        UPSERT driftConversationList;
        return true;
    }

    public static List<DriftData__Drift_Conversation__c> getRecordsToRestore(){
        List<DriftData__Drift_Conversation__c> records = [SELECT Id, Name, DriftData__Conversation_ID__c, DriftData__Inbox_ID__c, DriftData__Conversation_Start_Time__c, DriftData__Conversation_Close_Time__c
                                                          FROM DriftData__Drift_Conversation__c
                                                          WHERE Name LIKE 'HISTORY CONVERSATION %'];
        return records;
    }

    public static Boolean storeFetchingJobId(Id batchJobId){
        List<AsyncApexJob> jobsInfo = [SELECT Id
                                       FROM AsyncApexJob
                                       WHERE Id = :batchJobId];
        Id jobId = jobsInfo[0].Id;
        Id orgId = UserInfo.getOrganizationId();
        Drift_Jobs__c driftJobs = Drift_Jobs__c.getInstance(orgId);
        if (driftJobs.Fetching_Job_Id__c == null){
            driftJobs = new Drift_Jobs__c();
            driftJobs.SetupOwnerId = orgId;
        }
        driftJobs.Fetching_Job_Id__c = jobId;
        UPSERT driftJobs;
        return true;
    }

    public static Boolean storeRestoringJobId(Id batchJobId){
        AsyncApexJob jobInfo = getAsyncApexJobInfo(batchJobId);
        Id jobId = jobInfo.Id;
        Id orgId = UserInfo.getOrganizationId();
        Drift_Jobs__c driftJobs = Drift_Jobs__c.getInstance(orgId);
        if (driftJobs.Restoring_Job_Id__c == null){
            driftJobs = new Drift_Jobs__c();
            driftJobs.SetupOwnerId = orgId;
        }
        driftJobs.Restoring_Job_Id__c = jobId;
        UPSERT driftJobs;
        return true;
    }

    public static String getCurrentRestoringStatus(){
        RestoreStatus status = null;
        Id orgId = UserInfo.getOrganizationId();
        Drift_Jobs__c driftJobs = Drift_Jobs__c.getInstance(orgId);
        if (driftJobs.Fetching_Job_Id__c == null){
            status = RestoreStatus.Not_Started;
        } else{
            AsyncApexJob jobInfo = getAsyncApexJobInfo(driftJobs.Fetching_Job_Id__c);
            if (jobInfo.Status != 'Completed' && jobInfo.Status != 'Aborted'){
                status = RestoreStatus.Fetching;
            } else{
                if (driftJobs.Restoring_Job_Id__c == null){
                    status = RestoreStatus.Fetched;
                } else{
                    jobInfo = getAsyncApexJobInfo(driftJobs.Restoring_Job_Id__c);
                    if (jobInfo.Status != 'Completed' && jobInfo.Status != 'Aborted'){
                        status = RestoreStatus.Restoring;
                    } else{
                        status = RestoreStatus.Restored;
                    }
                }
            }
        }
        return status.name().replace('_', ' ');
    }

    public enum RestoreStatus{
        Not_Started, 
        Fetching, 
        Fetched, 
        Restoring, 
        Restored
    }

    private static AsyncApexJob getAsyncApexJobInfo(Id parameterId){
        AsyncApexJob jobInfo = [SELECT Id, Status
        FROM AsyncApexJob
        WHERE Id = :parameterId][0];
        return jobInfo;
    } 
}