import { LightningElement, api, wire } from 'lwc';
import fetchToRestore from '@salesforce/apex/HistoryFetchReportController.fetchToRestore';
import getCurrentStatus from '@salesforce/apex/HistoryFetchReportController.getCurrentStatus';
export default class HistoryFetchReport extends LightningElement {

    currentStatus=undefined;
    restoreDateValue=undefined;

    @api invoke(){
    }

    @wire(getCurrentStatus)
    wiredCurrentStatus(result){
        this.wiredCurrentStatusResult = result;
        const {data, error} = result;
        if(data){
            this.currentStatus = data;
            console.log(this.currentStatus);
            console.log(data);
        }
        if(error){
            console.log(error);
            this.currentStatus = undefined;
        }
    }

    connectedCallback(){
        
    }

    handleFetchRecordsClick(){
        fetchToRestore()
        .then(res=>{
            console.log(res);
        })
        .catch(err=>{
            console.log(err);
        });
    }

    handleRestoreRecordsClick(){

    }
}